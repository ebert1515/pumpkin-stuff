#include <Servo.h>
#include <Adafruit_NeoPixel.h>

// Servo variables
const int ServosAtOnce = 5; //number of servo's to move at once.
Servo ActiveServos[ServosAtOnce]; //only need as many ActiveServos objects as we're going to move at once.
const int ServoPin[] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}; //array of pins, doesn't need to be sequential
const int NumServos = (sizeof(ServoPin)/sizeof(int)); //number of int elements in array. since C++ doesn't have a count method.
long Servos[NumServos][4];

const int ServoMin = 10;
const int ServoMax = 160;
int TempServo;
bool found;

//motion detect variables
const int interruptPin = 21;
bool scared = false;
long lastseen;


//Behavior Variables
const int CalmSteps = 10; //number of steps when calm
const int CalmSpeed = 75; //milliseconds for each step. the combo of these creates a smooth slow movement
const int ScaredTime = 10; //seconds to stay scared once triggered


void setup() {
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), isawyou, CHANGE);
  Serial.begin(9600);

  //initialize Servos array
  for (int i = 0; i < NumServos; i++){
    Servos[i][0] = ServoPin[i]; //pin number from pin array
    Servos[i][1] = 90; //initial servo position 90*
    Servos[i][2] = 13132850; //initial color: 13132850=orange
  }
}

void isawyou() {
  Serial.println((String)"I SAW YOU!");
  lastseen = millis(); //set lastseen to current millis
}

void loop() {
  Serial.println((String)"Servos: " + NumServos);
   while (true) {

      //Determine if we're still scared.
      if (millis() - lastseen > (ScaredTime *1000)){
        scared = false;
      }else{
        scared = true;
      }
      //reset positions by init'ing array
      int ServoPositions[ServosAtOnce];
      
      //set the servo's and positions for the number or ServosAtOnce
     int UsedPins[ServosAtOnce];
     memset(UsedPins,-1,sizeof(UsedPins)); //set all UsedPins to -1
     for (int sp=0; sp< ServosAtOnce; sp++){

        //this is a terrible way to have to check for used pins, but Arduino C++ doesn't have a array.find method. ugg..
        do{
          TempServo = random(0,NumServos);
          found = false;
          for (int u=0; u<ServosAtOnce; u++){
            //Serial.println((String)TempServo + " == " + UsedPins[u]);
            if (TempServo == UsedPins[u]){
              found = true;
              Serial.println((String)"picking again, already using: " + TempServo);
            }
          }
        }while(found);
        UsedPins[sp] = TempServo;
        
        
        ActiveServos[sp].write(Servos[TempServo][1]); //set move position to last position
        ActiveServos[sp].attach(Servos[TempServo][0]); //attach to the servo, this should not move the servo is last position was right.
        ServoPositions[sp] = random(ServoMin,ServoMax);

        Servos[TempServo][1] = ServoPositions[sp]; //update servo position for next time.

        Serial.println((String)"attached Servo "+ sp + " to pin: " + TempServo + " Moving to " + ServoPositions[sp]);
      }
     
      if (scared){
        Serial.println("Freak out!");
        for (int sfo=0; sfo< ServosAtOnce; sfo++){
          //Servos[sfo][0].write(ServoPositions[sfo]);
          ActiveServos[sfo].write(ServoPositions[sfo]);
        }
      }else{
        //Serial.println("I aint scared");
        
        //find current position & define steps
        int ServoSteps[NumServos];
        for (int sok=0; sok< ServosAtOnce; sok++){
          ServoSteps[sok] = (ServoPositions[sok] - ActiveServos[sok].read())/CalmSteps;
          //Serial.println((String)"stepping servo" + sok + " by " + ServoSteps[sok]);
        }
                
        //step slowly
        for (int step=1; step<CalmSteps; step++){

          for (int ssok=0; ssok< ServosAtOnce; ssok++){
            int curpos = ActiveServos[ssok].read();
            //Servos[ssok].write(curpos + ServoSteps[ssok]);  // moves servoThree to random position
            ActiveServos[ssok].write(curpos + ServoSteps[ssok]);
          }
          delay(CalmSpeed); //step delay to move the eyes slowly. too much will cause nystagmus. Damn drunk pumpkins.
        }
      }
      Serial.println("sleeping....");
      delay(600); //this minimum delay may be needed to allow the servo to move into position

      //Detach all servos
      //Serial.println("Detaching all servos");
      for (int sreset=0; sreset< ServosAtOnce; sreset++){
      {
         ActiveServos[sreset].detach();
      }
   }
  }
}
