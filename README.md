# README #

## Pumpkin eyes ##

Servo actuated eyeballs in random order and random gaze.

### How do I get set up? ###

* wire servos to PWM pins (varies depending on Arduino model)
* configure the first couple lines of code to account for the number of pins, and pin numbers


### expectations ###

Eyes will start in "scared" mode, after about 10 seconds they'll settle down and move more slowly.
once motion is detected they'll move quickly again for about 10 seconds.

### Known issues ###

* on Servo.Attach() the servo moves to 90* position. this causes a quick movement when in "calm" mode.
I'd like to fix this, but it would require calling servo.write(previous) before attach, and would need to record every servo's last position.
if we're going to do that we might as well set up a big multi-dimensional array, and keep track of pin, position, and color all in one shot.

* the calm loop has a delay between each step, since some steps are bigger than others it can cause a stuttering motion. 


### Future ###

* addition of individually addressable LEDs, Like these: https://www.amazon.com/dp/B06XXCW65R/ref=nav_timeline_asin?_encoding=UTF8&psc=1
This will allow eyes to be colored and change when in scared mode.

Documentation on LEDs and libraries that can be used with Arduino:
https://www.tweaking4all.com/hardware/arduino/arduino-ws2812-led/
